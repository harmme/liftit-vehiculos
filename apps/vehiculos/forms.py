from .models import *
from django.forms import ModelForm
from django import forms


class MarcaForm(ModelForm):

    class Meta:
        model = Marca

        fields = [
            'nombre',
            'descripcion',
        ]

        widgets = {
            'nombre': forms.Field.widget(attrs={'class': 'form-control'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control'}),
        }


class TipoIdentificacionForm(ModelForm):

    class Meta:
        model = TipoIdentificacion

        fields = [
            'tipo',
            'tipo_corto',
            'juridica',
        ]

        widgets = {
            'tipo': forms.Field.widget(attrs={'class': 'form-control'}),
            'tipo_corto': forms.Field.widget(attrs={'class': 'form-control'}),
            'juridica': forms.CheckboxInput(attrs={'class': 'form-control'}),
        }


class PropietarioForm(ModelForm):

    class Meta:
        model = Propietario

        fields = [
            'nombre',
            'tipo_identificacion',
            'identificacion',
            'soporte_identificacion',
            'observaciones',
        ]

        widgets = {
            'nombre': forms.Field.widget(attrs={'class': 'form-control'}),
            'tipo_identificacion': forms.Select(attrs={'class': 'form-control required', 'style': 'width: 100%;'}),
            'identificacion': forms.Field.widget(attrs={'class': 'form-control'}),
            'soporte_identificacion': forms.FileInput,
            'observaciones': forms.Textarea(attrs={'class': 'form-control'}),
        }


class VehiculoForm(ModelForm):

    class Meta:
        model = Vehiculo

        fields = [
            'marca',
            'tipo',
            'propietario',
            'placa',
            'observaciones',
            'imagen',
        ]

        widgets = {
            'marca': forms.Select(attrs={'class': 'form-control required', 'style': 'width: 100%;'}),
            'tipo': forms.Select(attrs={'class': 'form-control required', 'style': 'width: 100%;'}),
            'propietario': forms.Select(attrs={'class': 'form-control required', 'style': 'width: 100%;'}),
            'placa': forms.Field.widget(attrs={'class': 'form-control'}),
            'observaciones': forms.Textarea(attrs={'class': 'form-control'}),
            'imagen': forms.FileInput,
        }


class ReporteVehiculoForm(ModelForm):

    class Meta:
        model = Vehiculo

        fields = [
            'marca',
        ]

        widgets = {
            'marca': forms.Select(attrs={'class': 'form-control required', 'style': 'width: 100%;'}),
        }