import os

from django.db import models


# Create your models here.
class Marca(models.Model):
    nombre = models.CharField(max_length=200, null=False, unique=True)
    descripcion = models.CharField(max_length=600, null=True)

    def __str__(self):
        return '%s' % self.nombre

    def cantidad(self):
        cantidad = self.vehiculo_set.count()

        return cantidad


class TipoIdentificacion(models.Model):
    tipo = models.CharField(max_length=100, null=False, verbose_name="Tipo de identificación")
    tipo_corto = models.CharField(max_length=5, null=False)
    juridica = models.BooleanField(default=False, null= False, blank=False)

    def __str__(self):
        return '%s - %s' % (self.tipo_corto, self.tipo)


class Propietario(models.Model):

    def upload_to(self,filename):
        name, ext = os.path.splitext(filename)
        return 'soporte-identificacion/{}{}'.format(self.identificacion, ext)

    nombre = models.CharField(max_length=200, null=False)
    tipo_identificacion = models.ForeignKey(TipoIdentificacion, null=False, blank=False, on_delete=None)
    identificacion = models.CharField(max_length=100, null=False, blank=False, primary_key=True)
    soporte_identificacion = models.FileField(upload_to=upload_to, max_length=200, null=True, blank=True, verbose_name="Soporte de identificacion")
    observaciones = models.CharField(max_length=600, null=True)

    def __str__(self):
        return '%s' % self.nombre


class TipoVehiculo(models.Model):
    tipo = models.CharField(max_length=100, null=False, verbose_name="Tipo de identificación")
    tipo_corto = models.CharField(max_length=5, null=False)

    def __str__(self):
        return '%s - %s' % (self.tipo_corto, self.tipo)


class Vehiculo(models.Model):

    def upload_to(self,filename):
        name, ext = os.path.splitext(filename)
        return 'imagen-vehiculo/{}{}'.format(self.id, ext)

    marca = models.ForeignKey(Marca, null=False, blank=False, on_delete=None)
    propietario = models.ForeignKey(Propietario, null=False, blank=False, on_delete=None)
    placa = models.CharField(max_length=600, null=True)
    tipo = models.ForeignKey(TipoVehiculo, null=False, blank=False, on_delete=None)
    observaciones = models.CharField(max_length=600, null=True)
    imagen = models.FileField(upload_to=upload_to, max_length=200, null=True, blank=True, verbose_name="Imagen del vehiculo")

    def __str__(self):
        return '%s - %s' % (self.marca, self.placa)
