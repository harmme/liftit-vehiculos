import json
import collections
import csv

from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.http import JsonResponse, HttpResponse
from rest_framework.views import APIView

from .models import *
from .forms import *
from .serializers import MarcaSerializer


class MarcaView(View):

    model = Marca
    template_name = 'vehiculos/main_marca.html'
    marca_form = MarcaForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class CreateMarca(CreateView):
    # Models
    model = Marca
    # Forms
    form_class = MarcaForm
    # Template
    template_name = 'vehiculos/add_marca.html'

    # Methods
    def get_context_data(self, **kwargs):
        context = super(CreateMarca, self).get_context_data(**kwargs)

        if 'form' not in context:
            context['form'] = self.form_class()

        return context

    def post(self, request, *args, **kwargs):
        response = JsonResponse({'status': False, 'message': "Error al agregar marca!"}, status=500)
        if request.POST.get('id') is None:
            marca_form = MarcaForm(request.POST)
            if marca_form.is_valid():
                marca_form.save()
                response = JsonResponse({'status': True, 'message': "Marca agregada correctmente"}, status=200)
        elif request.POST.get('id') is not None:
            marca = get_object_or_404(Marca, pk=request.POST.get('id'))
            marca_form = MarcaForm(request.POST, instance=marca)
            if marca_form.is_valid():
                marca_form.save()
                response = JsonResponse({'status': True, 'message': "Marca actualizada correctmente"}, status=200)

        return response


class ListMarca(ListView):
    model = Marca
    template_name = 'vehiculos/list_marca.html'
    proceso_form = MarcaForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'marcas': Marca.objects.all()})


class DetailMarca(DetailView):
    model = Marca
    template_name = 'vehiculos/detail_marca.html'

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', '')
        marca = Marca.objects.filter(pk=pk).first()

        return render(request, self.template_name, {'marca': marca})


class UpdateMarca(UpdateView):
    model = Marca
    template_name = 'vehiculos/update_marca.html'
    form_class = MarcaForm

    def get_context_data(self, **kwargs):
        context = super(UpdateMarca, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        context['pk'] = pk
        marca = self.model.objects.get(id=pk)

        if 'form' not in context:
            context['form'] = self.form_class(instance=marca)

        return context

    def post(self, request, *args, **kwargs):
        response = JsonResponse({'status': False, 'message': "Error al actualizar marca!"}, status=500)
        if request.POST.get('id') is not None:
            marca_form = MarcaForm(request.POST)
            if marca_form.is_valid():
                marca_form.save()
                response = JsonResponse({'status': True, 'message': "Marca actualizar correctmente"}, status=200)

        return response


class ReportMarca(View):

    template_name = 'vehiculos/report_marca.html'

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name)


class ReportMarcaAPI(APIView):
    serializer = MarcaSerializer

    def get(self, request, format=None, *args, **kwargs):
        lista = Marca.objects.all()
        response = self.serializer(lista, many=True)

        return JsonResponse(response.data, safe=False)


class PropietarioView(View):

    model = Propietario
    template_name = 'vehiculos/main_propietario.html'
    propietario_form = PropietarioForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class CreatePropietario(CreateView):
    # Models
    model = Propietario
    # Forms
    form_class = PropietarioForm
    # Template
    template_name = 'vehiculos/add_propietario.html'

    # Methods
    def get_context_data(self, **kwargs):
        context = super(CreatePropietario, self).get_context_data(**kwargs)

        if 'form' not in context:
            context['form'] = self.form_class()

        return context

    def post(self, request, *args, **kwargs):
        response = JsonResponse({'status': False, 'message': "Error al agregar propietario!"}, status=500)
        if request.POST.get('id') is None:
            propietario_form = PropietarioForm(request.POST, request.FILES)
            if propietario_form.is_valid():
                propietario_form.save()
                response = JsonResponse({'status': True, 'message': "Propietario agregado correctmente"}, status=200)
        elif request.POST.get('id') is not None:
            propietario = get_object_or_404(Propietario, pk=request.POST.get('id'))
            propietario_form = PropietarioForm(request.POST, request.FILES, instance=propietario)
            if propietario_form.is_valid():
                propietario_form.save()
                response = JsonResponse({'status': True, 'message': "Propietario actualizado correctmente"}, status=200)

        return response


class ListPropietario(ListView):
    model = Propietario
    template_name = 'vehiculos/list_propietario.html'
    proceso_form = PropietarioForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'propietarios': Propietario.objects.all()})


class DetailPropietario(DetailView):
    model = Propietario
    template_name = 'vehiculos/detail_propietario.html'

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', '')
        propietario = Propietario.objects.filter(identificacion=pk).first()

        return render(request, self.template_name, {'propietario': propietario})


class UpdatePropietario(UpdateView):
    model = Propietario
    template_name = 'vehiculos/update_propietario.html'
    form_class = PropietarioForm

    def get_context_data(self, **kwargs):
        context = super(UpdatePropietario, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        context['pk'] = pk
        propietario = self.model.objects.get(identificacion=pk)

        if 'form' not in context:
            context['form'] = self.form_class(instance=propietario)

        return context

    def post(self, request, *args, **kwargs):
        response = JsonResponse({'status': False, 'message': "Error al actualizar propietario!"}, status=500)
        if request.POST.get('id') is not None:
            propietario_form = PropietarioForm(request.POST)
            if propietario_form.is_valid():
                propietario_form.save()
                response = JsonResponse({'status': True, 'message': "Propietario actualizar correctmente"}, status=200)

        return response


class ExistPropietarioAPI(APIView):

    def post(self, request, format=None, *args, **kwargs):
        needle = request.POST.get('needle')
        cantidad = Propietario.objects.filter(identificacion=needle).count()
        if cantidad > 0:
            response = JsonResponse({'status': True, 'exist': 'true'}, status=200)
        else:
            response = JsonResponse({'status': True, 'exist': 'false'}, status=200)

        return response


class VehiculoView(View):

    model = Vehiculo
    template_name = 'vehiculos/main_vehiculo.html'
    vehiculo_form = VehiculoForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class CreateVehiculo(CreateView):
    # Models
    model = Vehiculo
    # Forms
    form_class = VehiculoForm
    # Template
    template_name = 'vehiculos/add_vehiculo.html'

    # Methods
    def get_context_data(self, **kwargs):
        context = super(CreateVehiculo, self).get_context_data(**kwargs)

        if 'form' not in context:
            context['form'] = self.form_class()

        return context

    def post(self, request, *args, **kwargs):
        response = JsonResponse({'status': False, 'message': "Error al agregar vehiculo!"}, status=500)
        if request.POST.get('id') is None:
            vehiculo_form = VehiculoForm(request.POST, request.FILES)
            if vehiculo_form.is_valid():
                vehiculo_form.save()
                response = JsonResponse({'status': True, 'message': "Vehiculo agregado correctmente"}, status=200)
        elif request.POST.get('id') is not None:
            vehiculo = get_object_or_404(Vehiculo, pk=request.POST.get('id'))
            vehiculo_form = VehiculoForm(request.POST, request.FILES, instance=vehiculo)
            if vehiculo_form.is_valid():
                vehiculo_form.save()
                response = JsonResponse({'status': True, 'message': "Vehiculo actualizado correctmente"}, status=200)

        return response


class ListVehiculo(ListView):
    model = Vehiculo
    template_name = 'vehiculos/list_vehiculo.html'
    proceso_form = VehiculoForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'vehiculos': Vehiculo.objects.all()})


class DetailVehiculo(DetailView):
    model = Vehiculo
    template_name = 'vehiculos/detail_vehiculo.html'

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', '')
        vehiculo = Vehiculo.objects.filter(pk=pk).first()

        return render(request, self.template_name, {'vehiculo': vehiculo})


class UpdateVehiculo(UpdateView):
    model = Vehiculo
    template_name = 'vehiculos/update_vehiculo.html'
    form_class = VehiculoForm

    def get_context_data(self, **kwargs):
        context = super(UpdateVehiculo, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        context['pk'] = pk
        vehiculo = self.model.objects.get(pk=pk)

        if 'form' not in context:
            context['form'] = self.form_class(instance=vehiculo)

        return context

    def post(self, request, *args, **kwargs):
        response = JsonResponse({'status': False, 'message': "Error al actualizar vehiculo!"}, status=500)
        if request.POST.get('id') is not None:
            vehiculo_form = VehiculoForm(request.POST)
            if vehiculo_form.is_valid():
                vehiculo_form.save()
                response = JsonResponse({'status': True, 'message': "Vehiculo actualizar correctmente"}, status=200)

        return response


class ReportVehiculo(View):

    template_name = 'vehiculos/report_vehiculo.html'
    form = ReporteVehiculoForm

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name, {'form': self.form})


class ReportVehiculoAPI(APIView):

    def post(self, request, format=None, *args, **kwargs):
        needle = request.POST.get('needle')
        lista = Vehiculo.objects.filter(marca_id=needle)
        array_data = []
        for vehiculo in lista:
            array_vehiculo = collections.OrderedDict()
            array_vehiculo['id'] = vehiculo.id
            array_vehiculo['marca'] = vehiculo.marca.nombre
            array_vehiculo['tipo'] = vehiculo.tipo.tipo
            array_vehiculo['propietario'] = vehiculo.propietario.nombre
            array_vehiculo['placa'] = vehiculo.placa
            array_vehiculo['observaciones'] = vehiculo.observaciones
            array_data.append(array_vehiculo)

        response = json.dumps(array_data)

        return HttpResponse(response)


class ExistVehiculoAPI(APIView):

    def post(self, request, format=None, *args, **kwargs):
        needle = request.POST.get('needle')
        cantidad = Vehiculo.objects.filter(placa=needle).count()
        if cantidad > 0:
            response = JsonResponse({'status': True, 'exist': 'true'}, status=200)
        else:
            response = JsonResponse({'status': True, 'exist': 'false'}, status=200)

        return response


class VehiculoCSV(View):

    model = Vehiculo

    def get(self, request, format=None, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="vehiculos.csv"'
        writer = csv.writer(response)
        writer.writerow([
            'marca',
            'tipo',
            'propietario',
            'placa',
            'observaciones',
        ])
        lista = self.model.objects.all()
        for vehiculo in lista:
            writer.writerow([
                vehiculo.marca.nombre,
                vehiculo.tipo.tipo,
                vehiculo.propietario.nombre,
                vehiculo.placa,
                vehiculo.observaciones,
            ])

        return response
