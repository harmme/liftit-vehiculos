from rest_framework.serializers import ModelSerializer
from .models import *


class MarcaSerializer(ModelSerializer):

    class Meta:
        model = Marca

        fields = (
            'nombre',
            'descripcion',
            'cantidad',
        )


class TipoIdentificacionSerializer(ModelSerializer):

    class Meta:
        model = TipoIdentificacion

        fields = (
            'tipo',
            'tipo_corto',
            'juridica',
        )


class PropietarioSerializer(ModelSerializer):

    class Meta:
        model = Propietario

        fields = (
            'nombre',
            'tipo_identificacion',
            'identificacion',
            'soporte_identificacion',
            'observaciones',
        )


class VehiculoSerializer(ModelSerializer):

    class Meta:
        model = Vehiculo

        fields = (
            'marca.nombre',
            'tipo.tipo',
            'propietario',
            'placa',
            'observaciones',
            'imagen',
        )
