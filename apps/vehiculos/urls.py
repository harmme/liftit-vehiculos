from django.urls import path
from .views import *

app_name = 'vehiculos'

urlpatterns = [
    path('marca/', MarcaView.as_view(), name="mainmarca"),
    path('marca/add', CreateMarca.as_view(), name="addmarca"),
    path('marca/list', ListMarca.as_view(), name="listmarca"),
    path('marca/detail/<int:pk>/', DetailMarca.as_view(), name="detailmarca"),
    path('marca/update/<int:pk>/', UpdateMarca.as_view(), name="updatemarca"),
    path('marca/report', ReportMarca.as_view(), name="reportmarca"),
    path('marca/report/api/', ReportMarcaAPI.as_view(), name="apireportmarca"),

    path('propietario/', PropietarioView.as_view(), name="mainpropietario"),
    path('propietario/add', CreatePropietario.as_view(), name="addpropietario"),
    path('propietario/list', ListPropietario.as_view(), name="listpropietario"),
    path('propietario/detail/<int:pk>/', DetailPropietario.as_view(), name="detailpropietario"),
    path('propietario/update/<int:pk>/', UpdatePropietario.as_view(), name="updatepropietario"),
    path('propietario/api/exist/', ExistPropietarioAPI.as_view(), name="apiexistpropietario"),

    path('vehiculo/', VehiculoView.as_view(), name="mainvehiculo"),
    path('vehiculo/add', CreateVehiculo.as_view(), name="addvehiculo"),
    path('vehiculo/list', ListVehiculo.as_view(), name="listvehiculo"),
    path('vehiculo/detail/<int:pk>/', DetailVehiculo.as_view(), name="detailvehiculo"),
    path('vehiculo/update/<int:pk>/', UpdateVehiculo.as_view(), name="updatevehiculo"),
    path('', ReportVehiculo.as_view(), name="reportvehiculo"),
    path('vehiculo/report/api/', ReportVehiculoAPI.as_view(), name="apireportvehiculo"),
    path('vehiculo/api/exist/', ExistVehiculoAPI.as_view(), name="apiexistvehiculo"),
    path('vehiculo/csv/', VehiculoCSV.as_view(), name="csvvehiculo"),
]