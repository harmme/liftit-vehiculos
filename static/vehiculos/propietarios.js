function loadTable(){
    $('#propietario-table-container').load('/vehiculos/propietario/list', function(){
        $('.btnDetail').click(function (e) {
            e.preventDefault();
            $('#addPropietarioModal').modal('show').find('.modal-dialog').load($(this).attr('href'));
        });
        $('.btnUpdate').click(function (e) {
            e.preventDefault();
            $('#addPropietarioModal').modal('show').find('.modal-dialog').load($(this).attr('href'), function() {
                $('#formPropietario').submit(function (e) {
                    e.preventDefault();
                    var formData = new FormData($(this)[0]);
                    $.ajax({
                        url: $(this).attr('action'),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success : function (json) {
                            $('#addPropietarioModal').modal('hide');
                            toastr.success(json.message);
                            loadTable();
                        },
                        error : function (xhr, errmsg, err) {
                             console.log(xhr.status+" : "+xhr.responseText+' errmsg '+errmsg);
                             toastr.error(json.message);
                        }
                    });
                });
            });
        });
    });
}

$('#btnAddPropietario').click(function (e) {
    e.preventDefault();
    $('#addPropietarioModal').modal('show').find('.modal-dialog').load($(this).attr('href'), function() {

        $('#formPropietario').submit(function (e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success : function (json) {
                    $('#addPropietarioModal').modal('hide');
                    toastr.success(json.message);
                    loadTable();
                },
                error : function (xhr, errmsg, err) {
                     console.log(xhr.status+" : "+xhr.responseText+' errmsg '+errmsg);
                     toastr.error(json.message);
                }
            });
        });
        /** Inputs init */
        $('#id_identificacion').blur(function(){
            $.ajax({
                url: '/vehiculos/propietario/api/exist/',
                type: "POST",
                data: {'needle': $(this).val()},
                dataType: 'json',
                success : function (json) {
                    if(json.exist == 'true'){
                        toastr.error('El Propietario ya se encuentra registrado');
                        $('#submitPropietario').prop('disabled', true);
                    }else{
                        $('#submitPropietario').prop('disabled', false);
                    }
                }
            });
        });
        /** Simple selects init */

    });
});

$(document).ready(function(){

    loadTable();

});