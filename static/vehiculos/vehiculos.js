function loadTable(){
    $('#vehiculo-table-container').load('/vehiculos/vehiculo/list', function(){
        $('.btnDetail').click(function (e) {
            e.preventDefault();
            $('#addVehiculoModal').modal('show').find('.modal-dialog').load($(this).attr('href'));
        });
        $('.btnUpdate').click(function (e) {
            e.preventDefault();
            $('#addVehiculoModal').modal('show').find('.modal-dialog').load($(this).attr('href'), function() {
                $('#formVehiculo').submit(function (e) {
                    e.preventDefault();
                    var formData = new FormData($(this)[0]);
                    $.ajax({
                        url: $(this).attr('action'),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success : function (json) {
                            $('#addVehiculoModal').modal('hide');
                            toastr.success(json.message);
                            loadTable();
                        },
                        error : function (xhr, errmsg, err) {
                             console.log(xhr.status+" : "+xhr.responseText+' errmsg '+errmsg);
                             toastr.error(json.message);
                        }
                    });
                });
            });
        });
    });
}

$('#btnAddVehiculo').click(function (e) {
    e.preventDefault();
    $('#addVehiculoModal').modal('show').find('.modal-dialog').load($(this).attr('href'), function() {

        $('#formVehiculo').submit(function (e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success : function (json) {
                    $('#addVehiculoModal').modal('hide');
                    toastr.success(json.message);
                    loadTable();
                },
                error : function (xhr, errmsg, err) {
                     console.log(xhr.status+" : "+xhr.responseText+' errmsg '+errmsg);
                     toastr.error(json.message);
                }
            });
        });
        /** Inputs init */
            $('#id_placa').blur(function(){
                $.ajax({
                    url: '/vehiculos/vehiculo/api/exist/',
                    type: "POST",
                    data: {'needle': $(this).val()},
                    dataType: 'json',
                    success : function (json) {
                        if(json.exist == 'true'){
                            toastr.error('La placa ya se encuentra registrada');
                            $('#submitVehiculo').prop('disabled', true);
                        }else{
                            $('#submitVehiculo').prop('disabled', false);
                        }
                    }
                });
            });
        /** Simple selects init */

    });
});

$(document).ready(function(){

    loadTable();

});