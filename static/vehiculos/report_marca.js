

function requestData() {
    $.ajax({
        url: '/vehiculos/marca/report/api/',
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#marcaReportData').html('');
            var html = '';
            var d = [];
            $.each(data, function (i, val) {
                var el = [];
                html += '<tr><td>' + val.nombre + '</td><td>' + val.cantidad + '</td></tr>';
                el[0] = val.nombre;
                el[1] = val.cantidad;
                d.push(el);
                console.log(val.nombre +':'+val.cantidad);
            });
            chart.addSeries({
                name: 'Vehiculos por marca',
                data: d,
                innerSize: '50%',
                type: 'pie'
            });
            $('#marcaReportData').html(html);
        },
        cache: false
    });
}


var chart = Highcharts.chart('marcaReportContainer', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: 'Vehiculos por Marca',
        align: 'center',
        verticalAlign: 'middle',
        y: 40
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%'
        }
    }
});

$(document).ready(function(){
    requestData();
});
