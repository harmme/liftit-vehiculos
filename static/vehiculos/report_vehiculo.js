$(document).ready(function(){
    $('#id_marca').select2({
            allowClear: true
        }).on('select2:select',function(e){
            var data = e.params.data;
            $.ajax({
                url: '/vehiculos/vehiculo/report/api/',
                type: 'POST',
                dataType: 'json',
                data: {'needle': data.id},
                success: function (data) {
                    $('#vehiculoReportData').html('');
                    var html = '';
                    $.each(data, function (i, val) {
                        var el = [];
                        html += '<tr><td>' + val.tipo + '</td><td>' + val.propietario + '</td><td>' + val.placa + '</td><td>' + val.observaciones + '</td></tr>';
                        console.log(val.tipo +':'+val.placa);
                    });
                    $('#vehiculoReportData').html(html);
                }
            });
        });
});