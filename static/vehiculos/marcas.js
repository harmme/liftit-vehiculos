function loadTable(){
    $('#marca-table-container').load('/vehiculos/marca/list', function(){
        $('.btnDetail').click(function (e) {
            e.preventDefault();
            $('#addMarcaModal').modal('show').find('.modal-dialog').load($(this).attr('href'));
        });
        $('.btnUpdate').click(function (e) {
            e.preventDefault();
            $('#addMarcaModal').modal('show').find('.modal-dialog').load($(this).attr('href'), function() {
                $('#formMarca').submit(function (e) {
                    e.preventDefault();
                    var formData = new FormData($(this)[0]);
                    $.ajax({
                        url: $(this).attr('action'),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success : function (json) {
                            $('#addMarcaModal').modal('hide');
                            toastr.success(json.message);
                            loadTable();
                        },
                        error : function (xhr, errmsg, err) {
                             console.log(xhr.status+" : "+xhr.responseText+' errmsg '+errmsg);
                             toastr.error(json.message);
                        }
                    });
                });
            });
        });
    });
}

$('#btnAddMarca').click(function (e) {
    e.preventDefault();
    $('#addMarcaModal').modal('show').find('.modal-dialog').load($(this).attr('href'), function() {

        $('#formMarca').submit(function (e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success : function (json) {
                    $('#addMarcaModal').modal('hide');
                    toastr.success(json.message);
                    loadTable();
                },
                error : function (xhr, errmsg, err) {
                     console.log(xhr.status+" : "+xhr.responseText+' errmsg '+errmsg);
                     toastr.error(json.message);
                }
            });
        });
        /** Inputs init */
        /** Simple selects init */

    });
});

$(document).ready(function(){

    loadTable();

});